package com.digitall.training.spiders.rms.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Repository;
import static com.mongodb.client.model.Filters.*;

import com.digitall.training.spiders.rms.entity.Candidate;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

@Repository
public class CandidateRepository {

    @Autowired
    MongoDbFactory mf;

    public List<Candidate> allCandidates() {
        List<Candidate> candidates = new ArrayList<>();
        CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        Consumer<Candidate> c = e -> candidates.add(e);

        mf.getDb("pavithra").getCollection("candidates", Candidate.class).withCodecRegistry(codec).find().forEach(c);
        return candidates;
    }

    public Candidate findCandidate(long candidateId) {
        Candidate temp = null;
        CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoCollection collection = mf.getDb("pavithra").getCollection("candidates", Candidate.class).withCodecRegistry(codec);
        FindIterable<Candidate> iterate = collection.find(eq("candidateId", candidateId), Candidate.class);
        for (Candidate c : iterate) {
            temp = c;
        }
        return temp;
    }

    public boolean addCandidate(Candidate candidate) {
        try {
            CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                    CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
            mf.getDb("pavithra").getCollection("candidates", Candidate.class).withCodecRegistry(codec)
                    .insertOne(candidate);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateCandidate(long id, Candidate candidate) {
        try {
            CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                    CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
            mf.getDb("pavithra").getCollection("candidates", Candidate.class).withCodecRegistry(codec)
                    .replaceOne(eq("candidateId", id), candidate);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteCandidateById(long id) {
        try {
            CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                    CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
            mf.getDb("pavithra").getCollection("candidates", Candidate.class).withCodecRegistry(codec)
                    .deleteOne(eq("candidateId", id));
            return true;
        } catch (MongoException t) {
            t.printStackTrace();
        }
        return false;
    }
}
