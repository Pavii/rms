package com.digitall.training.spiders.rms.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.digitall.training.spiders.rms.entity.Interview;

@Repository
public class InterviewRepository {
	@Autowired
	DataSource datasource;

	public boolean addInterview(Interview interview) {
		try (Connection con = datasource.getConnection();
				PreparedStatement pst = con.prepareStatement("INSERT INTO interview VALUES( ?,?,?, ?, ?, ?, ?, ?)")) {

			pst.setLong(1, interview.getCandidateId());
			pst.setString(2, interview.getModeOfInterview());
			pst.setDate(3, interview.getDate());
			pst.setString(4, interview.getLocation());
			pst.setString(5, interview.getRecommendation());
			pst.setString(6, interview.getInterviewPanel());
			pst.setString(7, interview.getReference());
                         pst.setString(8,interview.getName());
			pst.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean updateInterviewList(Interview interview, long candidateId) {
		try (Connection con = datasource.getConnection();
				PreparedStatement pst = con
						.prepareStatement("UPDATE interview SET mode_of_interview=?, date=?, location=?,"
								+ "recommendation=?,interview_panel=?,reference=?,name=?" + "WHERE candidate_id=?")) {

            
			pst.setString(1, interview.getModeOfInterview());
			pst.setDate(2, interview.getDate());
			pst.setString(3, interview.getLocation());
			pst.setString(4, interview.getRecommendation());
			pst.setString(5, interview.getInterviewPanel());
			pst.setString(6, interview.getReference());
                        pst.setString(7,interview.getName());
                        pst.setLong(8, candidateId);
			pst.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean deleteInterviewlist(long candidateId) {
		try (Connection con = datasource.getConnection();
				PreparedStatement pst = con.prepareStatement("delete from interview where candidate_id=?")) {
			pst.setLong(1, candidateId);
			pst.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<Interview> allInterviewList() {
		List<Interview> iList = new ArrayList<>();
		try (Connection con = datasource.getConnection();
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("SELECT * FROM interview")) {
			while (rs.next()) {
				iList.add(new Interview(rs.getLong(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getString(7),rs.getString(8)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iList;
	}
}
