package com.digitall.training.spiders.rms.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitall.training.spiders.rms.entity.Interview;
import com.digitall.training.spiders.rms.repository.InterviewRepository;

@Service
public class InterviewService {
	@Autowired
	InterviewRepository ir;
	
	public List<Interview> getAllInterviewList() throws SQLException{
		return ir.allInterviewList();
	}
	public boolean addInterview(Interview interview) {
		return ir.addInterview(interview);
	}
	public boolean updateInterview(Interview interview, long candidate_id) {
		return ir.updateInterviewList(interview,candidate_id);
	}
	public boolean deleteInterview(long candidate_id) {
		return ir.deleteInterviewlist(candidate_id);
	}
}
