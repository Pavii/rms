	package com.digitall.training.spiders.rms.entity;

import java.sql.Date;

public class Interview {
	private long candidateId;
        private String name;
	private String modeOfInterview;
	private Date date;
	private String location;
	private String recommendation;
	private String interviewPanel;
	private String reference;

	public Interview() {
		super();
	}

	public Interview(long candidateId, String modeOfInterview, Date date, String location, String recommendation,
			String interviewPanel, String reference,String name) {
		super();
		this.candidateId = candidateId;
		this.modeOfInterview = modeOfInterview;
		this.date = date;
		this.location = location;
		this.recommendation = recommendation;
		this.interviewPanel = interviewPanel;
		this.reference = reference;
                this.name=name;
	}
       
	public long getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(long candidateId) {
		this.candidateId = candidateId;
	}
        public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModeOfInterview() {
		return modeOfInterview;
	}

	public void setModeOfInterview(String modeOfInterview) {
		this.modeOfInterview = modeOfInterview;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	public String getInterviewPanel() {
		return interviewPanel;
	}

	public void setInterviewPanel(String interviewPanel) {
		this.interviewPanel = interviewPanel;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

}