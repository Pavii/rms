package com.digitall.training.spiders.rms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import javax.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.digitall.training.spiders.rms.entity.Candidate;
import com.digitall.training.spiders.rms.services.CandidateService;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/candidates")
public class CandidateController {

    private final static Logger LOGGER = Logger.getLogger(CandidateController.class.getName());
    @Autowired
    CandidateService cservice;
    String errMsg = "Incomplete or Invalid Data:";

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Candidate> getAllCandidates() {
        SampleLogger.fileLogging();
        LOGGER.log(Level.INFO, "View Candidate List");
        return cservice.getAllCandidatesList();
    }

    @PostMapping
    public ResponseEntity addCandidate(@RequestBody @Valid Candidate candidate,
            BindingResult binding) {
        if (!binding.hasErrors() && cservice.addCandidate(candidate)) {
            return ResponseEntity.ok("Successfully added");
        } else {
            return ResponseEntity.ok("Aadhar id already exist");
        }
    }

    @PutMapping
    public ResponseEntity<String> updateCandidate(@RequestBody Candidate candidate, BindingResult binding) {
        if (!binding.hasErrors() && cservice.updateCandidate(candidate.getCandidateId(), candidate)) {
            return ResponseEntity.ok("Successfully Updated");
        } else {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body(errMsg + binding.toString());
        }
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<String> deleteCandidateById(@PathVariable long id) {
        if (cservice.deleteCandidateById(id)) {
            return ResponseEntity.ok("Successfully Deleted");
        } else {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(errMsg);
        }
    }

//	@DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> deleteCandidateById(@RequestParam("candidateId") int id) {
//		if (cservice.deleteCandidateById(id)) {
//			return ResponseEntity.ok("Successfully Deleted");
//		} else {
//			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(errMsg);
//		}
//	}
    @ModelAttribute("candidate")
    public Candidate setCandidate() {
        return new Candidate();
    }
}
