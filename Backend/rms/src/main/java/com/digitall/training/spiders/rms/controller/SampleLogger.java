/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitall.training.spiders.rms.controller;

import com.digitall.training.spiders.rms.controller.CandidateController;
import java.io.IOException;
import java.util.logging.*;

/**
 *
 * @author GR-18
 */
public class SampleLogger {

    private final static Logger logger = Logger.getLogger(CandidateController.class.getName());

    public static void fileLogging(){
        try {
            FileHandler filehandler = new FileHandler("H:/RMS/candidateLog.log", true);
            logger.addHandler(filehandler);
            SimpleFormatter formatter = new SimpleFormatter();
            filehandler.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
