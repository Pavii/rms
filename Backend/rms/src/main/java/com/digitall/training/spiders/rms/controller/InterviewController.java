package com.digitall.training.spiders.rms.controller;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.digitall.training.spiders.rms.entity.Interview;
import com.digitall.training.spiders.rms.services.InterviewService;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/interview")
public class InterviewController {
	@Autowired
	InterviewService iservice;
	String errMsg = "Incomplete or Invalid Data:";
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Interview> getAllInterviewList() throws SQLException {
		return iservice.getAllInterviewList();
	}

	@PostMapping
	public ResponseEntity<String> addInterview(@RequestBody Interview interview,
			BindingResult binding) {
		if (!binding.hasErrors() && iservice.addInterview(interview)) {
			return ResponseEntity.ok("Successfully added");
		} else {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body( errMsg + binding.toString());
		}
	}

	@PutMapping
	public ResponseEntity<String> updateInterview(@RequestBody Interview interview, BindingResult binding) {
		if (!binding.hasErrors() && iservice.updateInterview(interview,interview.getCandidateId())) {
			return ResponseEntity.ok("Successfully Updated");
		} else {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(errMsg + binding.toString());
		}
	}

	@DeleteMapping({"/{id}"})
	public ResponseEntity<String> deleteInterview(@PathVariable long id) {
		if (iservice.deleteInterview(id)) {
			return ResponseEntity.ok("Successfully Deleted");
		} else {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(errMsg);
		}
	}

	@ModelAttribute("interview")
	public Interview setInterview() {
		return new Interview();
	}
}