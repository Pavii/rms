package com.digitall.training.spiders.rms.entity;
import javax.validation.constraints.*;

public class Candidate {
//        @UniqueElements
	private long candidateId;
	private String firstName;
	private String lastName;
	private String mobile;
	private String alternativeMobile;
        @Email
	private String email;
	private int totalExperience;
	private int relevantExperience;
	private String skills;
	private String address;
	private String qualification;
	private double expectedCtc;
	private double currentCtc;
	private String positionApplied;
	private String gender;
	private String dob;

	public Candidate() {
		super();
	}

	public Candidate(long candidateId, String firstName, String lastName, String mobile, String alternativeMobile,
			String email, int totalExperience, int relevantExperience, String skills,
			String address, String qualification, double expectedCtc, double currentCtc,
			String positionApplied, String gender, String dob) {
		super();
		this.candidateId = candidateId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobile = mobile;
		this.alternativeMobile = alternativeMobile;
		this.email = email;
		this.totalExperience = totalExperience;
		this.relevantExperience = relevantExperience;
		this.skills = skills;
		this.address = address;
		this.qualification = qualification;
		this.expectedCtc = expectedCtc;
		this.currentCtc = currentCtc;
		this.positionApplied = positionApplied;
		this.gender = gender;
		this.dob = dob;
	}

	public long getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(long candidateId) {
		this.candidateId = candidateId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAlternativeMobile() {
		return alternativeMobile;
	}

	public void setAlternativeMobile(String alternativeMobile) {
		this.alternativeMobile = alternativeMobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTotalExperience() {
		return totalExperience;
	}

	public void setTotalExperience(int totalExperience) {
		this.totalExperience = totalExperience;
	}

	public int getRelevantExperience() {
		return relevantExperience;
	}

	public void setRelevantExperience(int relevantExperience) {
		this.relevantExperience = relevantExperience;
	}

	public String getSkills() {
		return skills;
	}
        public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
        
	public double getExpectedCtc() {
		return expectedCtc;
	}

	public void setExpectedCtc(double expectedCtc) {
		this.expectedCtc = expectedCtc;
	}

	public double getCurrentCtc() {
		return currentCtc;
	}

	public void setCurrentCtc(double currentCtc) {
		this.currentCtc = currentCtc;
	}

	public String getPositionApplied() {
		return positionApplied;
	}

	public void setPositionApplied(String positionApplied) {
		this.positionApplied = positionApplied;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

}