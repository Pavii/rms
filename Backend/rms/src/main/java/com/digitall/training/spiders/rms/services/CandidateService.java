package com.digitall.training.spiders.rms.services;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitall.training.spiders.rms.entity.Candidate;
import com.digitall.training.spiders.rms.repository.CandidateRepository;
import com.digitall.training.spiders.rms.repository.InterviewRepository;

@Service
public class CandidateService {
	@Autowired
	CandidateRepository cr;

        @Autowired
        InterviewRepository ir;
        
	public List<Candidate> getAllCandidatesList() {
		return cr.allCandidates();
	}

	public boolean updateCandidate(long id, Candidate candidate) {
		return cr.updateCandidate(id, candidate);
	}
        
        public boolean deleteCandidateById(long id){
            if(cr.deleteCandidateById(id)){
                ir.deleteInterviewlist(id);
                return true;
            }
            return false;
        }
        public boolean addCandidate(Candidate candidate) {
            Candidate temp= cr.findCandidate(candidate.getCandidateId());
            if(temp==null){
                cr.addCandidate(candidate);
            return true;
            }
            else{
		return false;
	}
        }
        
}
